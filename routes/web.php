<?php

Route::group(['middleware' => 'prevent-back-history'], function () {
    Auth::routes();

    Route::get('logout', ['middleware' => 'auth', 'as' => 'logout', 'uses' => 'Auth\LoginController@logout']);

    /* |--------------------------------------------------- HOME ---------------------------------------------------- */
    Route::get('/', 'HomeController@index')->middleware('auth')->name('home.index');
    Route::post('/', 'HomeController@store')->middleware('auth')->name('home.store');


    Route::get('/user', 'UserController@index')->middleware('auth')->name('user.index');
    Route::get('/user/create', 'UserController@create')->middleware('auth')->name('user.create');
    Route::post('user/create', 'UserController@store')->middleware('auth')->name('user.create');

    Route::get('/summary', 'SummaryController@index')->middleware('auth')->name('summary.index');

});