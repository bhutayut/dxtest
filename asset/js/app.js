var discount = 0;
$(document).ready(function () {
    create_date_time();
    /* ------------------------------------------------ dataTables ------------------------------------------------ */

    $('.dataTables').DataTable({
        pageLength: 10,
        responsive: true,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {extend: 'copy'},
            {extend: 'csv'},
            {extend: 'excel', title: 'ExportFile'},
            {
                extend: 'pdf', title: 'ExportFile', orientation: 'landscape',
                pageSize: 'A4'
            },
            {
                extend: 'print',
                orientation: 'landscape',
                pageSize: 'A4',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table')
                        .addClass('compact')
                        .css('font-size', 'inherit');
                }
            }
        ]
    });

    /* ------------------------------------------------ select2 ------------------------------------------------ */

    $(".select2").select2({
        placeholder: "Select a Value",
        allowClear: true
    });

    /* ------------------------------------------------ Dropzone ------------------------------------------------ */

    Dropzone.options.dropzoneForm = {
        paramName: "file", // The name that will be used to transfer the file
        maxFilesize: 2, // MB
        dictDefaultMessage: "<strong>Drop files here or click to upload. </strong></br> (This is just a demo dropzone. Selected files are not actually uploaded.)"
    };

    /* ------------------------------------------------ datepicker ------------------------------------------------ */
    $('.datepicker').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true,
        format: "yyyy-mm-dd"
    });

    $('#data_5 .input-daterange').datepicker({
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true,
        format: "yyyy-mm-dd"
    });

    $('input[name="daterange"]').daterangepicker();

    $('#reportrange span').html(moment().subtract(29, 'days').format('yyyy-mm-dd') + ' - ' + moment().format('yyyy-mm-dd'));

    $('#reportrange').daterangepicker({
        format: 'YYYY-MM-DD',
        startDate: moment().subtract(29, 'days'),
        endDate: moment(),
        minDate: '2000-01-01',
        maxDate: '3000-12-31',
        dateLimit: {days: 60},
        showDropdowns: true,
        showWeekNumbers: true,
        timePicker: false,
        timePickerIncrement: 1,
        timePicker12Hour: true,
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        opens: 'right',
        drops: 'down',
        buttonClasses: ['btn', 'btn-sm'],
        applyClass: 'btn-primary',
        cancelClass: 'btn-default',
        separator: ' to ',
        locale: {
            applyLabel: 'Submit',
            cancelLabel: 'Cancel',
            fromLabel: 'From',
            toLabel: 'To',
            customRangeLabel: 'Custom',
            daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
            monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
            firstDay: 1
        }
    }, function (start, end, label) {
        console.log(start.toISOString(), end.toISOString(), label);
        $('#reportrange span').html(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
    });

    /* ------------------------------------------------ Datatable ------------------------------------------------ */


});

function create_date_time() {
    var dtime = moment().format('YYYY-MM-DD HH:mm:ss');

    if (document.getElementById("dt")) {
        document.getElementById("dt").innerHTML = dtime;
        $(".date_time").val(dtime);
    }

    setTimeout(function () {
        create_date_time();
    }, 1000);
}

/* ------------------------------------------------ Get Details From User ------------------------------------------------ */

$('.getJSON').change(function () {
    setDataToFields(this);
});

function setDataToFields(obj) {
    var url = $(obj).attr("data-link") + '/' + $(obj).val();
    var fields = $(obj).attr("data-fields");

    $.getJSON(url, function (data) {
        //console.log(data);
        $(".user-details-1").html('');
        $(".user-details-1").append('<label class="col-sm-4 col-md-4 control-label"> Discount </label><label class="col-sm-4 col-md-4 control-label">' + data.customer_discount_rate + ' %</label>');
        $("#customer_name").val(data.customer_name);
        discount = parseFloat(data.customer_discount_rate);

        calPrice();
        $(".user-details-2").html('');
        if (data.prohibit_client) {
            $(".user-details-2").append('<label class="col-sm-4 col-md-4 control-label"> Prohibit </label><label class="col-sm-4 col-md-4 control-label text-danger"> Yes </label>');
        }

        $("#payment_type").select2('val', data.customer_payment_type);
    });
}

/* ------------------------------------------------ testData ------------------------------------------------ */

function testData() {

    $('input[type=number].qty').each(function () {
        $(this).val(randomNumeric());
    });

    $('input[type=number]').not('.qty').each(function () {
        $(this).val(randomNumeric());
    });

    $('input[type=tel]').each(function () {
        $(this).val(randomTel());
    });

    $('input[type=text],textarea').not('.datepicker,.time,.number,.prevent')
        .each(function () {
            $(this).val(randomString());
        });

    var a = [];

    $("select.select2").each(function () {

        $(this).find("option").each(function () {
            a.push($(this).val());
        });

        a.shift();

        var rnum = Math.floor(Math.random() * (a.length - 1));
        $(this).select2().select2("val", a[rnum]);
        a = [];
    });
}

function randomNumeric() {

    var nums = "0123456789";
    var double_length = 3;
    var precision_length = 0;
    var randomnumeric = '';
    for (var i = 0; i < double_length; i++) {
        var rnum = Math.floor(Math.random() * nums.length);
        randomnumeric += nums.substring(rnum, rnum + 1);
    }

    var randomprecision = '';
    for (var i = 0; i < double_length; i++) {
        var rnum = Math.floor(Math.random() * nums.length);
        randomprecision += nums.substring(rnum, rnum + 1);
    }

    return randomnumeric;// + '.' + randomprecision;
}

function randomString() {

    var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";
    var string_length = 10;
    var randomstring = '';
    for (var i = 0; i < string_length; i++) {
        var rnum = Math.floor(Math.random() * chars.length);
        randomstring += chars.substring(rnum, rnum + 1);
    }
    return randomstring;
}

$('.product-images').slick({
    dots: true
});


function randomTel() {

    var length = 9;
    return '0' + Math.floor(Math.pow(10, length - 1) + Math.random() * 9 * Math.pow(10, length - 1));
}


$('#confirmDelete').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget);
    var id = button.data('id');
    var url = button.data('url');

    var modal = $(this);
    modal.find('#confirm').attr("href", url + id);
})


$('#confirmApprove').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget);
    var id = button.data('id');
    var url = button.data('url');

    var modal = $(this);
    modal.find('#confirm').attr("href", url + id);
})
