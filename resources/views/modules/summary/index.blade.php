@extends('layouts.app')
@section('module_name')
    Home
@endsection
@section('current')
    Index
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">

            @if ($errors->any())
                <script> toastr.error("{{ implode('', $errors->all(':message')) }}", "", {
                        "closeButton": true,
                        "debug": true,
                        "progressBar": true,
                        "preventDuplicates": false,
                        "positionClass": "toast-top-right",
                        "showDuration": "5000",
                        "hideDuration": "5000",
                        "timeOut": "5000",
                        "extendedTimeOut": "5000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                    }); </script>
            @endif

            <div class="wrapper wrapper-content">


                <div class="row">
                    <div class="col-lg-12">

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="ibox float-e-margins">
                                    <div class="ibox-title">
                                        รายการคำร้องในเดือนนี้
                                    </div>
                                    <div class="ibox-content">
                                        <table class="table table-hover no-margins" id="datatable">
                                            <thead>
                                            <tr>
                                                <th>#</th>

                                                <th>ชื่อ-สกุล</th>
                                                <th>ระดับ</th>
                                                <th>แผนก</th>
                                                <th>Manager</th>

                                                <th>จำนวน (THB)</th>


                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php $i = 1; ?>
                                            @foreach($items as $item)
                                                <tr>
                                                    <td>
                                                        <small>{{ $i++ }}</small>
                                                    </td>

                                                    <td>{{ $item['firstname'] }} {{ $item['lastname'] }}</td>
                                                    <td>{{ $item['level'] }}</td>
                                                    <td>{{ $item['department'] }}</td>
                                                    <td>{{ $item['man_firstname'] }} {{ $item['man_lastname'] }}</td>

                                                    <td>{{ number_format($item['amount'], 2, '.', ',')}}</td>

                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                        </div>


                    </div>
                </div>
            </div>

        </div>
    </div>




    <div class="modal inmodal" id="createRequest" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content animated bounceInRight">

                <form method="post" class="form-horizontal">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="modal-header">
                        <h2 id="title"></h2>
                    </div>

                    <div id="modalBody" class="modal-body">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-12">
                                    <input class="form-control" type="hidden" id="cash_status_code"
                                           name="cash_status_code">
                                    <input class="form-control" type="hidden" id="code" name="code">
                                    <input class="form-control" type="hidden" id="username" name="username">
                                    <div class="form-group">
                                        <label for="unit">จำนวนเงิน : </label>
                                        <input class="form-control" type="number" step="any" min="0"
                                               max="99999999" id="amount" name="amount">
                                    </div>
                                    <div class="form-group">
                                        <label for="description">รายละเอียด : </label>
                                        <textarea class="form-control" cols="40" rows="5" id="description"
                                                  name="description"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button id="submitButton" class="btn btn-primary" type="submit" name="submit"><i
                                    class="fa fa-save"></i> ยืนยัน
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@section('script')

    <script>
        $(document).ready(function () {
            var table = $('#datatable').DataTable({});
        });


        $(document).on("click", ".openDialog", function () {

            if ($(this).data('status') == 'EDIT' || $(this).data('status') == 'REQUEST') {
                $(".modal-body #amount").attr('disabled', false);
                $(".modal-body #description").attr('disabled', false);
            } else {
                $(".modal-body #amount").attr('disabled', true);
                $(".modal-body #description").attr('disabled', true);
            }

            switch ($(this).data('status')) {
                case 'REQUEST':
                    $(".modal-header #title").html('สร้างคำร้อง');
                    break;
                case 'APPROVE':
                    $(".modal-header #title").html('อนุมัติคำร้อง');
                    break;
                case 'DECLINE':
                    $(".modal-header #title").html('ปฏิเสธิคำร้อง');
                    break;
                case 'CANCEL':
                    $(".modal-header #title").html('ยกเลิกคำร้อง');
                    break;
                case 'EDIT':
                    $(".modal-header #title").html('แก้ไขคำร้อง');
                    break;
            }

            $(".modal-body #code").val($(this).data('code'));
            $(".modal-body #username").val($(this).data('username'));
            $(".modal-body #cash_status_code").val($(this).data('status') == 'EDIT' ? 'REQUEST' : $(this).data('status'));
            $(".modal-body #amount").val($(this).data('amount') ? $(this).data('amount') : '');
            $(".modal-body #description").val($(this).data('description') ? $(this).data('description') : '');

        });

        $('form').submit(function () {
            $(".modal-body #amount").attr('disabled', false);
            $(".modal-body #description").attr('disabled', false);

            $(this).find(':submit').attr('disabled', 'disabled');
        });
    </script>
@endsection

