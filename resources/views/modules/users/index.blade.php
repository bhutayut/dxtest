@extends('layouts.app')
@section('module_name')
    User
@endsection
@section('current')
    Index
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="wrapper wrapper-content">
                <div class="row animated fadeInDown">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            @if(\App\User::isAdmin(Auth::user()->username))
                                @include('layouts.partials.ibox-title')
                            @endif

                            @if ($errors->any())
                                <script> toastr.error("{{ implode('', $errors->all(':message')) }}", "", {
                                        "closeButton": true,
                                        "debug": true,
                                        "progressBar": true,
                                        "preventDuplicates": false,
                                        "positionClass": "toast-top-right",
                                        "showDuration": "5000",
                                        "hideDuration": "5000",
                                        "timeOut": "5000",
                                        "extendedTimeOut": "5000",
                                        "showEasing": "swing",
                                        "hideEasing": "linear",
                                        "showMethod": "fadeIn",
                                        "hideMethod": "fadeOut"
                                    }); </script>
                            @endif

                            <div class="ibox-content">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover dataTables">
                                        <thead>
                                        <tr>
                                            <th class="hidden-xs hidden-md">@</th>
                                            <th>คำนำหน้า</th>
                                            <th>ชื่อ</th>
                                            <th>นามสกุล</th>
                                            <th>ชื่อเล่น</th>
                                            <th>มือถือ</th>
                                            <th>แผนก</th>
                                            <th>level</th>
                                            <th>Status</th>
                                            <th>#</th>
                                        </tr>
                                        <thead>
                                        <tbody>
                                        @foreach($items as $item)

                                            <tr>
                                                <td class="hidden-xs hidden-md">{{ $item->id }}</td>
                                                <td>{{ $item->title}}</td>
                                                <td>{{ $item->firstname}}</td>
                                                <td>{{ $item->lastname }}</td>
                                                <td>{{ $item->nickname }}</td>
                                                <td>{{ $item->mobile }}</td>
                                                <td>{{ $item->department}}</td>
                                                <td>{{ $item->level}}</td>
                                                <td>{{ $item->user_status}}</td>
                                                <td>
                                                    @if(\App\User::isAdmin(Auth::user()->username))
                                                        {{--  <div class="btn-group">
                                                              <a href="{{ URL::to('users/edit/'.Crypt::encryptString($item->username))}}"
                                                                 class="btn btn-warning btn-flat btn-sm btn-edit-item"
                                                                 title="Edit"><i
                                                                          class="fa fa-edit"></i></a>
                                                              <a data-toggle="modal" data-target="#confirmDelete"
                                                                 data-id="{{Crypt::encryptString($item->username)}}"
                                                                 data-url="users/delete/"
                                                                 class="btn btn-danger btn-flat btn-sm btn-delete-item"
                                                                 href="#"
                                                                 title="Delete"><i class="fa fa-trash-o"></i></a>
                                                          </div>--}}
                                                </td>
                                                @endif
                                            </tr>
                                        @endforeach

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')

    <script>
        $(document).ready(function () {
            $('form').submit(function () {
                $(this).find(':submit').attr('disabled', 'disabled');
            });

        });
    </script>
@endsection

