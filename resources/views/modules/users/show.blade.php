@extends('layouts.app')
@section('module_name')
    Teacher
@endsection
@section('current')
    Show
@endsection
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="wrapper wrapper-content">
                <div class="row animated fadeInDown">

                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">

                            @include('layouts.partials.ibox-title')

                            <div class="ibox-content">
                                <div class="wrapper wrapper-content animated fadeInRight">

                                    @foreach($items as $item)

                                        <div class="row m-b-lg m-t-lg">
                                            <div class="col-md-4">

                                                <div class="profile-image">
                                                    <img src="@if($item->img_url) {{ URL::asset($item->img_url) }} @else {{ URL::asset('/asset/img/user.svg') }} @endif"
                                                         class="img-circle circle-border m-b-md"
                                                         alt="profile">
                                                </div>
                                                <div class="profile-info">
                                                    <div class="">
                                                        <div>
                                                            <h1 class="no-margins">
                                                                {{ $item->firstname or '' }} {{ $item->lastname or '' }}
                                                                ({{ $item->nickname or '' }})
                                                            </h1>
                                                            <h2 class="no-margins">
                                                                #{{ $item->username or '' }}
                                                            </h2>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="col-md-2">
                                                <small>จำนวนเงินที่ได้รับเดือนนี้</small>
                                                <h1 class="no-margins">{{ number_format($total, 2, '.', ',')}}
                                                    บาท</h1>


                                            </div>
                                            <div class="col-md-2">
                                                <small>ยอดเงินสะสมปัจจุบัน</small>
                                                <h1 class="no-margins">{{ number_format($current_saving + (($total/10)+(($total/10)*0.03)), 2, '.', ',') }}
                                                    บาท

                                                </h1>
                                                <small>
                                                    ยกยอดจาก {{ number_format($current_saving, 2, '.', ',')}} บาท
                                                </small>
                                              <!--  @if(\App\User::isAdmin(Auth::user()->username))
                                                    <a data-toggle="modal" href="#updateSaving"
                                                       data-username="{{Crypt::encryptString($item->username)}}"
                                                       data-unit="{{$item->saving}}"
                                                       class="btn btn-warning btn-flat btn-sm btn-edit-item open-updateSavingDialog"
                                                       title="Edit"><i class="fa fa-edit"></i>
                                                        แก้ไข</a>@endif -->


                                            </div>

                                            @if(\App\User::isAdmin(Auth::user()->username))
                                                <div class="col-md-2">
                                                    <small>ค่าประสบการณ์</small>
                                                    <h1 class="no-margins">{{ number_format($item->bonus, 2, '.', ',')}}
                                                        %<br/>
                                                        @if(\App\User::isAdmin(Auth::user()->username))
                                                            <a data-toggle="modal" href="#updateExp"
                                                               data-username="{{Crypt::encryptString($item->username)}}"
                                                               data-unit="{{$item->bonus}}"
                                                               class="btn btn-warning btn-flat btn-sm btn-edit-item open-updateExpDialog"
                                                               title="Edit"><i class="fa fa-edit"></i>
                                                                แก้ไข</a>@endif

                                                    </h1>


                                                </div>
                                            @endif
                                            <div class="col-md-2">
                                                <small>ชั่วโมงสอน เดือนนี้</small>
                                                <h1 class="no-margins">@if($thisMonthHour>0){{ $thisMonthHour}}@else
                                                        0 @endif
                                                    ชั่วโมง</h1>
                                                <small>
                                                    ชั่วโมงสอนทั้งหมด @if($totalhour){{ $totalhour or 0}}@endif
                                                    ชั่วโมง
                                                </small>

                                            </div>
                                        </div>

                                    @endforeach

                                    <div class="row">

                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered table-hover dataTables">
                                                <thead>
                                                <tr>

                                                    <th>#</th>
                                                    <th>วันที่</th>
                                                    <th>เริ่มเวลา</th>
                                                    <th>ถึงเวลา</th>
                                                    <th>ชั่วโมง</th>

                                                    <th>วิชา</th>
                                                    <th>กลุ่ม</th>
                                                    <th>หัวข้อ</th>


                                                    <th>รหัส</th>
                                                    <th>ชื่อเล่น</th>

                                                    <th>ประเภท</th>
                                                    <th>Qty</th>
                                                    <th>ค่าสอน/ชม.</th>
                                                    <th>รวม</th>

                                                    <th>หมายเหตุ</th>

                                                </tr>
                                                <thead>

                                                <tbody>
                                                <?php $i = 1;?>



                                                @if(count($lists))
                                                    @foreach($lists as $list)
                                                        <?php $k = 1;?>
                                                        @foreach($list as $item)


                                                            <tr>
                                                                <td>{{ $i }}</td>
                                                                <td>{{ $item['course_date'] }}</td>
                                                                <td>{{ $item['start_time'] }}</td>
                                                                <td>{{ $item['end_time'] }}</td>
                                                                <td>{{ $item['hour_time'] }}</td>

                                                                <td>{{ $item['course_name']}}</td>
                                                                <td>{{ $item['group_name']}}</td>
                                                                <td>{{ $item['topic']}}</td>

                                                                <td>{{ $item['student_id'] or '' }}</td>
                                                                <td>{{ $item['nickname'] or ''  }}</td>

                                                                <td>
                                                                    @if($item['action_type_code'] =='USE')
                                                                        {{'หัก'}}
                                                                    @elseif($item['action_type_code'] =='GET')
                                                                        {{'ค่าสอน'}}
                                                                    @elseif($item['action_type_code'] =='CANCEL')
                                                                        {{'ยกเลิก'}}
                                                                    @elseif($item['action_type_code'] =='WHOLE')
                                                                        {{'เหมาจ่าย'}}
                                                                    @elseif($item['action_type_code'] =='TRY')
                                                                        {{'ทดลองสอน'}}
                                                                    @endif</td>

                                                                <td>{{ $item['qty'] or '-' }}</td>
                                                                <td>{{ $item['wage'] or '-' }}</td>
                                                                <td>@if($k==1){{ $item['unit'] }}@endif</td>
                                                                <td>{{ $item['remark'] }}
                                                                    @if(\App\User::isAdmin(Auth::user()->username))
                                                                        {{--@if($item['action_type_code'] =='WHOLE')--}}
                                                                        <a data-toggle="modal" href="#updateUnit"
                                                                           data-id="{{ $item['a_id'] }}"
                                                                           data-timetable_id="{{ $item['t_id'] }}"
                                                                           data-date="{{ $item['course_date'] }}"
                                                                           data-start="@if($item['action_type_code'] !='TOPUP' && $item['action_type_code'] !='TRANSFER')
                                                                           {{ $item['start_time'] }} @endif"
                                                                           data-end="@if($item['action_type_code'] !='TOPUP' && $item['action_type_code'] !='TRANSFER')
                                                                           {{ $item['end_time'] }}
                                                                           @endif"
                                                                           data-course="{{ $item['course_name'] }}"
                                                                           data-topic="{{ $item['topic'] }}"
                                                                           data-hour="{{ $item['hour_time'] }}"
                                                                           data-type="{{ $item['action_type_code'] }}"
                                                                           data-unit="{{ $item['wage'] }}"
                                                                           class="btn btn-warning btn-flat btn-sm btn-edit-item open-updateDialog"
                                                                           title="Edit"><i class="fa fa-edit"></i>
                                                                            แก้ไข</a>
                                                                        {{--@endif--}}
                                                                    @endif
                                                                </td>
                                                            </tr>

                                                            <?php $k++; ?>

                                                            <?php $i++;?>
                                                        @endforeach
                                                    @endforeach
                                                @else
                                                    <tr>
                                                        <td colspan="12" class="center">ไม่พบข้อมูล</td>
                                                    </tr>
                                                @endif

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal inmodal" id="updateUnit" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content animated bounceInRight">


                <form method="post" class="form-horizontal">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span>
                            <span class="sr-only">ปิด</span></button>
                        <h4 id="title-txt"></h4>
                    </div>

                    <div id="modalBody" class="modal-body">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-4">
                                    <input class="form-control" type="hidden" id="id" name="id">
                                    <div class="form-group">
                                        <label for="topic">วันที่ : </label> <span id="date"></span>

                                    </div>
                                    <div class="form-group">
                                        <label for="topic">เวลา : </label> <span id="start"></span>-<span
                                                id="end"></span>
                                    </div>
                                    <div class="form-group">
                                        <label for="topic">วิชา : </label> <span id="course"></span>

                                    </div>
                                    <div class="form-group">
                                        <label for="topic">หัวข้อ : </label> <span id="topic"></span>

                                    </div>
                                    <div class="form-group">
                                        <label for="topic">จำนวนชั่วโมง : </label> <span id="hour_text"></span>
                                        <input class="form-control" type="hidden" id="hour" name="hour">
                                        <input class="form-control" type="hidden" id="timetable_id" name="timetable_id">

                                    </div>


                                </div>
                                <div class="col-md-6 col-md-offset-1">

                                    <div class="form-group">
                                        <label for="topic">ประเภท : </label>
                                        <select class="form-control" id="type" name="type">
                                            <option value="GET" class="type get">ค่าสอน</option>
                                            <option value="WHOLE" class="type whole">เหมาจ่าย</option>
                                            <option value="TRY" class="type try">ทดลองสอน</option>
                                        </select>
                                    </div>


                                    <div class="form-group">
                                        <label for="topic">ค่าสอน/ชั่วโมง (บาท) : </label>
                                        <input class="form-control" type="number" step="any" min="0"
                                               max="999999" id="unit" name="unit">
                                    </div>


                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="modal-footer">

                        <button id="submitButton" class="btn btn-primary" type="submit" name="submit" value="WHOLE"><i
                                    class="fa fa-save"></i> แก้ไข
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal inmodal" id="updateSaving" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content animated bounceInRight">
                <form method="post" class="form-horizontal">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div id="modalBody" class="modal-body">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-6 col-md-offset-3">
                                    <input class="form-control" type="hidden" id="username" name="username">
                                    <div class="form-group">
                                        <label for="topic">ยอดเงินสะสม (บาท) : </label>
                                        <input class="form-control" type="number" step="any" min="0"
                                               max="9999999" id="unit" name="unit">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button id="submitButton" class="btn btn-primary" type="submit" name="submit" value="SAVING"><i
                                    class="fa fa-save"></i> แก้ไข
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal inmodal" id="updateExp" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content animated bounceInRight">
                <form method="post" class="form-horizontal">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div id="modalBody" class="modal-body">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-6 col-md-offset-3">
                                    <input class="form-control" type="hidden" id="username" name="username">
                                    <div class="form-group">
                                        <label for="topic">ค่าประสบการณ์ (%) : </label>
                                        <input class="form-control" type="number" step="any" min="0"
                                               max="200" id="unit" name="unit">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button id="submitButton" class="btn btn-primary" type="submit" name="submit" value="EXP"><i
                                    class="fa fa-save"></i> แก้ไข
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')

    <script>
        $(document).ready(function () {

            $(document).on("click", ".open-updateDialog", function () {
                $(".modal-body #id").val($(this).data('id'));

                if ($(this).data('start')) {

                    $(".modal-body #date").html($(this).data('date'));
                    $(".modal-body #start").html($(this).data('start'));
                    $(".modal-body #end").html($(this).data('end'));
                    $(".modal-body #topic").html($(this).data('topic'));
                    $(".modal-body #course").html($(this).data('course'));

                } else {
                    $(".modal-body #topic").html('ยกยอด');
                }

                $(".modal-body #timetable_id").val($(this).data('timetable_id'));
                $(".modal-body #hour_text").html($(this).data('hour'));
                $(".modal-body #hour").val(parseFloat($(this).data('hour')));
                $(".modal-body #unit").val(parseFloat($(this).data('unit')));


                $(".modal-body #type .get").removeClass('hide');
                $(".modal-body #type .whole").removeClass('hide');
                $(".modal-body #type .try").removeClass('hide');

                if ($(this).data('type') == 'WHOLE') {
                    $(".modal-body #type").val($(this).data('type'));
                    $(".modal-body #type .get").addClass('hide');
                    $(".modal-body #type .try").addClass('hide');

                } else {
                    $(".modal-body #type").val($(this).data('type'));
                    $(".modal-body #type .whole").addClass('hide');
                }


            });

            $(document).on("click", ".open-updateSavingDialog", function () {
                $(".modal-body #username").val($(this).data('username'));
                $(".modal-body #unit").val(parseFloat($(this).data('unit')));
            });

            $(document).on("click", ".open-updateExpDialog", function () {
                $(".modal-body #username").val($(this).data('username'));
                $(".modal-body #unit").val(parseFloat($(this).data('unit')));
            });

        });
    </script>
@endsection

