@extends('layouts.app')
@section('module_name')
    User
@endsection
@section('current')
    Create
@endsection

@section('content')

    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">

                    @include('layouts.partials.ibox-title')

                    <div class="ibox-content">

                        @if ($errors->any())
                            <script> toastr.error("{{ implode('', $errors->all(':message')) }}", "", {
                                    "closeButton": true,
                                    "debug": true,
                                    "progressBar": true,
                                    "preventDuplicates": false,
                                    "positionClass": "toast-top-right",
                                    "showDuration": "5000",
                                    "hideDuration": "5000",
                                    "timeOut": "5000",
                                    "extendedTimeOut": "5000",
                                    "showEasing": "swing",
                                    "hideEasing": "linear",
                                    "showMethod": "fadeIn",
                                    "hideMethod": "fadeOut"
                                }); </script>
                        @endif

                        <form method="post" class="form-horizontal">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <div class="row">

                                <div class="col-md-4 col-md-offset-4">
                                    @if($items)
                                        <input type="hidden" name="id"
                                               value="@if($items){{$items[0]->id}}@endif">
                                        <input type="hidden" name="username"
                                               value="@if($items){{$items[0]->username}}@endif">
                                        <input type="hidden" name="email"
                                               value="@if($items){{$items[0]->email}}@endif">
                                        <input type="hidden" name="password"
                                               value="@if($items){{$items[0]->password}}@endif">

                                        <div class="form-group">
                                            <label for="name">Username: {{$items[0]->username}}</label>
                                        </div>
                                        <div class="form-group">
                                            <label for="name">Email: {{$items[0]->email}}</label>
                                        </div>
                                        <div class="form-group">
                                            <label for="password">Password:</label>
                                            <input type="password" class="form-control" id="password" name="password"
                                                   placeholder="min:6">
                                        </div>

                                        <div class="form-group">
                                            <label for="password_confirmation">Password Confirmation:</label>
                                            <input type="password" class="form-control" id="password_confirmation"
                                                   placeholder="confirm email"
                                                   name="password_confirmation">
                                        </div>
                                    @else
                                        <div class="form-group">
                                            <label for="name">Username:</label>
                                            <input type="text" class="form-control" id="username" name="username"
                                                   placeholder="min:4 | max:20" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="email">Email:</label>
                                            <input type="email" class="form-control" id="email" name="email"
                                                   placeholder="email" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="password">Password:</label>
                                            <input type="password" class="form-control" id="password" name="password"
                                                   placeholder="min:6" required>
                                        </div>

                                        <div class="form-group">
                                            <label for="password_confirmation">Password Confirmation:</label>
                                            <input type="password" class="form-control" id="password_confirmation"
                                                   placeholder="confirm email"
                                                   name="password_confirmation" required>
                                        </div>

                                        <div class="form-group">
                                            <label for="title">กำหนดสิทธิ์ : </label>
                                            <select class="form-control" name="role_id" id="role_id" required>
                                                <option value=""> --- เลือกกลุ่ม ---</option>
                                                @foreach($roles as $item)
                                                    <option value="{{ $item->id }}">{{ $item->name}}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    @endif


                                    <div class="form-group">
                                        <label for="title">แผนก : </label>
                                        <select class="form-control" name="department_id" id="department_id" required>
                                            <option value=""> --- เลือกกลุ่ม ---</option>
                                            @foreach($departments as $item)
                                                <option value="{{ $item->id }}">{{ $item->name}}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="title">ระดับ : </label>
                                        <select class="form-control" name="level_id" id="level_id" required>
                                            <option value=""> --- เลือกกลุ่ม ---</option>
                                            @foreach($levels as $item)
                                                <option value="{{ $item->id }}">{{ $item->name}}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="title">สถานะ : </label>
                                        <select class="form-control" name="user_status_id" id="user_status_id" required>
                                            <option value=""> --- เลือกกลุ่ม ---</option>
                                            @foreach($user_status as $item)
                                                <option value="{{ $item->id }}">{{ $item->name}}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>


                                    <div class="form-group">
                                        <label for="title">คำนำหน้า : </label>
                                        <select class="form-control" name="title_code" id="title_code" required>
                                            <option value=""> --- เลือกกลุ่ม ---</option>
                                            @foreach($titles as $item)
                                                <option value="{{ $item->code }}">{{ $item->name}}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="title">Manager : </label>
                                        <select class="form-control" name="manager_code" id="manager_code" required>
                                            <option value=""> --- เลือกกลุ่ม ---</option>
                                            @foreach($managers as $item)
                                                <option value="{{ $item->username }}">{{ $item->firstname}}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="name">Firstname:</label>
                                        <input type="text" class="form-control" id="firstname" name="firstname"
                                               required value="@if($items){{$items[0]->firstname}}@endif">
                                    </div>
                                    <div class="form-group">
                                        <label for="name">Lastname:</label>
                                        <input type="text" class="form-control" id="lastname" name="lastname"
                                               value="@if($items){{$items[0]->lastname}}@endif">
                                    </div>
                                    <div class="form-group">
                                        <label for="name">Nickname:</label>
                                        <input type="text" class="form-control" id="nickname" name="nickname" required
                                               value="@if($items){{$items[0]->nickname}}@endif">
                                    </div>
                                    <div class="form-group">
                                        <label for="name">Mobile:</label>
                                        <input type="tel" class="form-control" id="mobile" name="mobile"
                                               value="@if($items){{$items[0]->mobile}}@endif">
                                    </div>


                                </div>
                            </div>

                            <div class="row">
                                <div class="hr-line-dashed"></div>
                                <div class="form-group">
                                    <div class="col-md-6 col-md-offset-5">

                                        <a class="btn btn-white"
                                           href="{{ URL::to('/')}}/{{strtolower(trim(View::yieldContent('module_name')))}}">
                                            Cancel
                                        </a>
                                        <button style="cursor:pointer" type="submit" class="btn btn-primary">Submit
                                        </button>

                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')


    <script>
        $(document).ready(function () {
            $('form').submit(function () {
                $(this).find(':submit').attr('disabled', 'disabled');
            });


        });
    </script>
@endsection

