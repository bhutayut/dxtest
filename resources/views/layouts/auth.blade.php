<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->

<html lang="{{ config('app.locale') }}">

@section('htmlheader')
    @include('layouts.partials.htmlheader')
@show


<body>

<div id="wrapper">
    <!-- Main content -->
    <section class="content">
        <!-- Your Page Content Here -->
        @yield('content')
    </section><!-- /.content -->

</div><!-- ./page-wrapper -->

</div><!-- ./wrapper -->

</body>

</html>