<div class="footer">
    <div class="pull-right">
        <?php
        $total_time = round((microtime(TRUE) - $_SERVER['REQUEST_TIME_FLOAT']), 7);
        echo $total_time . " ms.";
        ?>
    </div>
    <div>
        <strong>Copyright</strong> {{ config('app.name', 'Laravel') }} Ver.{{ config('app.version', 'Demo') }} &copy; 2018
    </div>
</div>