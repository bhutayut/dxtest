<div class="ibox-title">
    @if(strtolower(trim(View::yieldContent('current'))) == "index")
        <a class="btn btn-primary btn-nest"
           href="{{ URL::to('/'.strtolower(trim(View::yieldContent('module_name'))))}}/create">
            <i class="fa fa-plus"></i> เพิ่ม
        </a>
    @else
        <a class="btn btn-warning btn-nest"
           href="{{ url()->previous() }}">
            <i class="fa fa-arrow-left"></i> กลับ
        </a>
    @endif
    <div class="ibox-tools">
        <a class="collapse-link">
            <i class="fa fa-chevron-up"></i>
        </a>

    </div>
</div>
