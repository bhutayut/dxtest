<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }} {{ config('app.version', 'Demo') }}</title>

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>

    <link href="{{ URL::asset('/asset/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('/asset/font-awesome/css/font-awesome.css') }}" rel="stylesheet">

    <!-- Toastr style -->
    <link href="{{ URL::asset('/asset/css/plugins/toastr/toastr.min.css') }}" rel="stylesheet">

    <!-- Gritter -->
    <link href="{{ URL::asset('/asset/js/plugins/gritter/jquery.gritter.css') }}" rel="stylesheet">

    <!-- Data Table -->
    <link href="{{ URL::asset('/asset/css/plugins/dataTables/datatables.min.css')}}" rel="stylesheet">

    <link href="{{ URL::asset('/asset/css/plugins/iCheck/custom.css')}}" rel="stylesheet">

    <!-- Select 2 -->
    <link href="{{ URL::asset('/asset/css/plugins/select2/select2.min.css') }}" rel="stylesheet">

    <!-- dropzone 2 -->
    <link href="{{ URL::asset('/asset/css/plugins/dropzone/dropzone.css') }}" rel="stylesheet">

    <link href="{{ URL::asset('/asset/css/plugins/slick/slick.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('/asset/css/plugins/slick/slick-theme.css') }}" rel="stylesheet">

    <link href="{{ URL::asset('/asset/css/plugins/daterangepicker/daterangepicker-bs3.css') }}" rel="stylesheet">

    <!-- datepicker 3 -->
    <link href="{{ URL::asset('/asset/css/plugins/datapicker/datepicker3.css') }}" rel="stylesheet">

    <!-- datepicker 3 -->
    <link href="{{ URL::asset('/asset/js/plugins/schedule/schedule.css') }}" rel="stylesheet">

    <link href="{{ URL::asset('/asset/css/plugins/timepicker/timepicker.min.css') }}" rel="stylesheet"/>


    <!-- Animate -->
    <link href="{{ URL::asset('/asset/css/animate.css') }}" rel="stylesheet">

    <link href="{{ URL::asset('/asset/css/style.css') }}" rel="stylesheet">

    <link href="{{ URL::asset('/asset/css/app.css') }}" rel="stylesheet">

    <script src="{{ URL::asset('/asset/js/jquery-3.1.1.min.js') }}"></script>

    <script src="{{ URL::asset('/asset/js/bootstrap.min.js') }}"></script>
    <script src="{{ URL::asset('/asset/js/plugins/metisMenu/jquery.metisMenu.js') }}"></script>
    <script src="{{ URL::asset('/asset/js/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>

    <!-- Calendar -->
    <link href="{{ URL::asset('/asset/css/plugins/fullcalendar/fullcalendar.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('/asset/css/plugins/fullcalendar/fullcalendar.print.css') }}" rel='stylesheet'
          media='print'>

    {{--<link href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.print.css" rel='stylesheet' media='print'>--}}

    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

    <!-- Toastr -->
    <script src="{{ URL::asset('/asset/js/plugins/toastr/toastr.min.js') }}"></script>
    <!-- Custom -->


</head>