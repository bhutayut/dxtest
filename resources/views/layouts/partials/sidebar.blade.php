<div class="row border-bottom white-bg">
    <nav class="navbar navbar-static-top" role="navigation">
        <div class="navbar-header">
            <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse"
                    class="navbar-toggle collapsed" type="button">
                <i class="fa fa-reorder"></i>
            </button>
            <a href="#" class="navbar-brand">{{ config('app.name', 'Laravel') }}</a>
        </div>
        <div class="navbar-collapse collapse" id="navbar">
            <ul class="nav navbar-nav">
                <li class="@if(strtolower(trim(View::yieldContent('module_name'))) =="home") {{ "active" }} @endif">
                    <a href="{{ URL::to('/')}}" aria-expanded="false"
                       role="button"><span class="nav-label"><i class="fa fa-home fa-1x"></i> หน้าหลัก</span></a>
                </li>

                @if(\App\User::isFin(Auth::user()->username))
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <span class="hidden-xs"><i class="fa fa-money fa-fw"></i> จัดการข้อมูลการเบิกจ่าย</span>
                        </a>
                        <ul role="menu" class="dropdown-menu">
                            <li class="@if(strtolower(trim(View::yieldContent('module_name'))) =="summary") {{ "active" }} @endif">
                                <a href="{{ URL::to('summary')}}" aria-expanded="false"
                                   role="button"><span class="nav-label"><i
                                                class="fa fa-sellsy"></i> สรุปยอดเงินสําหรับฝ่ายการเงิน</span></a>
                            </li>
                        </ul>
                    </li>
                @endif

                @if(\App\User::isAdmin(Auth::user()->username))
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <span class="hidden-xs"><i class="fa fa-users"></i> จัดการข้อมูล</span>
                        </a>
                        <ul role="menu" class="dropdown-menu">
                            <li class="@if(strtolower(trim(View::yieldContent('module_name'))) =="user") {{ "active" }} @endif">
                                <a href="{{ URL::to('user')}}" aria-expanded="false"
                                   role="button"><span class="nav-label"><i
                                                class="fa fa-clock-o"></i> จัดการข้อมูลผู้เใช้งาน</span></a>
                            </li>

                        </ul>
                    </li>
                @endif

            </ul>
            <ul class="nav navbar-top-links navbar-right">
                <li>
                    <a href="#"><span id='dt'></span></a>
                </li>

                <!-- User Account Menu -->
                <li class="dropdown">

                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-user-circle"></i>
                        <!-- hidden-xs hides the username on small devices so only the image appears. -->
                        <span class="hidden-xs">{{ Auth::user()->username }}</span>
                    </a>

                    <ul role="menu" class="dropdown-menu">
                        @if(\App\User::isAdmin(Auth::user()->username))
                            <li><a class="btn bg-orange btn-flat" href="#" onClick="testData();">Random</a></li>
                        @endif
                        <li><a href="{{ URL::to('/logout')}}" class="btn btn-default btn-flat">Sign out</a></li>
                    </ul>
                </li>

            </ul>
        </div>
    </nav>
</div>

