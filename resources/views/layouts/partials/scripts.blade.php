<!-- REQUIRED JS SCRIPTS -->
<!-- Mainly scripts -->
<!-- jQuery 3.1.1 -->


<!-- Flot -->
<script src="{{ URL::asset('/asset/js/plugins/flot/jquery.flot.js') }}"></script>
<script src="{{ URL::asset('/asset/js/plugins/flot/jquery.flot.tooltip.min.js') }}"></script>
<script src="{{ URL::asset('/asset/js/plugins/flot/jquery.flot.spline.js') }}"></script>
<script src="{{ URL::asset('/asset/js/plugins/flot/jquery.flot.resize.js') }}"></script>
<script src="{{ URL::asset('/asset/js/plugins/flot/jquery.flot.pie.js') }}"></script>

<!-- Peity -->
<script src="{{ URL::asset('/asset/js/plugins/peity/jquery.peity.min.js') }}"></script>
<script src="{{ URL::asset('/asset/js/demo/peity-demo.js') }}"></script>

<!-- Custom and plugin javascript -->
<script src="{{ URL::asset('/asset/js/inspinia.js') }}"></script>
<script src="{{ URL::asset('/asset/js/plugins/pace/pace.min.js') }}"></script>

<!-- jQuery UI -->
<script src="{{ URL::asset('/asset/js/plugins/jquery-ui/jquery-ui.min.js') }}"></script>

<!-- GITTER -->
<script src="{{ URL::asset('/asset/js/plugins/gritter/jquery.gritter.min.js') }}"></script>

<!-- Sparkline -->
<script src="{{ URL::asset('/asset/js/plugins/sparkline/jquery.sparkline.min.js') }}"></script>

<!-- Sparkline demo data  -->
<script src="{{ URL::asset('/asset/js/demo/sparkline-demo.js') }}"></script>

<!-- ChartJS-->
<script src="{{ URL::asset('/asset/js/plugins/chartJs/Chart.min.js') }}"></script>


<!-- Datatables -->
<script src="{{ URL::asset('/asset/js/plugins/dataTables/datatables.min.js')}}"></script>

<!-- Select2 -->
{{--<script src="{{ URL::asset('/asset/js/plugins/select2/select2.full.min.js')}}"></script>--}}

<!-- DROPZONE -->
<script src="{{ URL::asset('/asset/js/plugins/dropzone/dropzone.js')}}"></script>

<!-- slick carousel-->
<script src="{{ URL::asset('/asset/js/plugins/slick/slick.min.js')}}"></script>

<!-- Data picker -->
<script src="{{ URL::asset('/asset/js/plugins/datapicker/bootstrap-datepicker.js')}}"></script>

<!-- moment -->
<script src="{{ URL::asset('/asset/js/plugins/moment/moment.min.js')}}"></script>

<!-- Date range picker -->
<script src="{{ URL::asset('/asset/js/plugins/daterangepicker/daterangepicker.js')}}"></script>

<!-- Full Calendar -->
<script src="{{ URL::asset('/asset/js/plugins/fullcalendar/fullcalendar.min.js')}}"></script>
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.min.js"></script>--}}


<!-- iCheck -->
<script src="{{ URL::asset('/asset/js/plugins/iCheck/icheck.min.js') }}"></script>

<script src="{{ URL::asset('/asset/js/plugins/timepicker/timepicker.min.js') }}"></script>

<!-- jq.schedule -->
<script src="{{ URL::asset('/asset/js/plugins/schedule/jq.schedule.js')}}"></script>

<!-- Custom -->
<script src="{{ URL::asset('/asset/js/app.js')}}"></script>

@yield('script')

<script>

    $(window).on("load", function () {
        // Animate loader off screen
        $(".se-pre-con").fadeOut("slow");
    });
</script>