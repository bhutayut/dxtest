<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-12">
        <h2>@yield('module_name', 'Page Header here')</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ URL::to('/')}}">Home</a>
            </li>
            <li>
                <a href="{{ URL::to('/')}}/{{str_replace(" ","_",strtolower(trim(View::yieldContent('module_name'))))}}">@yield('module_name')</a>
            </li>
            <li class="active">
                <strong>@yield('current')</strong>
            </li>

        </ol>
    </div>
</div>