@if (session('SUCCESS'))
    <script> toastr.success("{{session('SUCCESS')}}", "SUCCESS", {
            "closeButton": true,
            "debug": true,
            "progressBar": true,
            "preventDuplicates": false,
            "positionClass": "toast-top-right",
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "3000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }); </script>
@endif
@if (session('UPDATE'))
    <script> toastr.success("{{session('UPDATE')}}", "SUCCESS", {
            "closeButton": true,
            "debug": true,
            "progressBar": true,
            "preventDuplicates": false,
            "positionClass": "toast-top-right",
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "3000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }); </script>
@endif
@if (session('DELETE'))
    <script> toastr.error("{{session('DELETE')}}", "SUCCESS", {
            "closeButton": true,
            "debug": true,
            "progressBar": true,
            "preventDuplicates": false,
            "positionClass": "toast-top-right",
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "3000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }); </script>
@endif
@if (session('FAILED'))
    <script> toastr.error("{{session('FAILED')}}", "", {
            "closeButton": true,
            "debug": true,
            "progressBar": true,
            "preventDuplicates": false,
            "positionClass": "toast-top-right",
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "3000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }); </script>
@endif
@if (session('ERROR'))
    <script>toastr.error('ITEM DOES NOT EXISTS', 'ERROR!!'); </script>
@endif

<div class="modal inmodal" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                            class="sr-only">Close</span></button>
                <i class="fa fa-laptop modal-icon"></i>
                <h4 class="modal-title">Modal title</h4>
                <small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                </small>
            </div>
            <div class="modal-body">
                <p><strong>Lorem Ipsum is simply dummy</strong> text of the printing and typesetting industry. Lorem
                    Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown
                    printer took a galley of type and scrambled it to make a type specimen book. It has survived not
                    only five centuries, but also the leap into electronic typesetting,
                    remaining essentially unchanged.</p>
                <div class="form-group"><label>Sample Input</label> <input type="email" placeholder="Enter your email"
                                                                           class="form-control"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>


<div id="confirmDelete" class="modal fade" tabindex="-2" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title">ลบข้อมูล</h4>
            </div>
            <div class="modal-body">
                <p>
                    คุณต้องการลบข้อมูลนี้ ใช่หรือไม่ ?
                </p>
            </div>
            <div class="modal-footer">
                <a class="btn btn-default" data-dismiss="modal"> ยกเลิก </a>
                <a class="btn btn-danger" id="confirm"> ยืนยัน </a>
            </div>
        </div>
    </div>
</div>

<div id="confirmActive" class="modal fade" tabindex="-2" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title">Active</h4>
            </div>
            <div class="modal-body">
                <p>
                    Do you want to active this row ?
                </p>
            </div>
            <div class="modal-footer">
                <a class="btn btn-default" data-dismiss="modal"> Cancel </a>
                <a class="btn btn-danger" id="confirm"> Yes </a>
            </div>
        </div>
    </div>
</div>

<div id="confirmDeactive" class="modal fade" tabindex="-2" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title">De-active</h4>
            </div>
            <div class="modal-body">
                <p>
                    Do you want to de-active this row ?
                </p>
            </div>
            <div class="modal-footer">
                <a class="btn btn-default" data-dismiss="modal"> Cancel </a>
                <a class="btn btn-danger" id="confirm"> Yes </a>
            </div>
        </div>
    </div>
</div>

<div id="confirmPaid" class="modal fade" tabindex="-2" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Change Confirmation</h4>
            </div>
            <div class="modal-body">
                <p>Are you sure you, want to confirm about</p>
                <p name="display_id"></p>
                <p> Baht is already paid.</p>
                <input type="hidden" name="item_id" id="item_id">
                <input type="hidden" name="url" id="url">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-primary" id="confirm-btn">Confirm</button>
                <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div id="confirmApprove" class="modal fade" tabindex="-2" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title">ยืนยันข้อมูล</h4>
            </div>
            <div class="modal-body">
                <p>
                    คุณต้องการยืนยันว่า ได้ตรวจสอบความถูกต้องของข้อมูลนี้แล้ว ใช่หรือไม่ ?
                </p>
            </div>
            <div class="modal-footer">
                <a class="btn btn-default" data-dismiss="modal"> ยกเลิก </a>
                <a class="btn btn-success" id="confirm"> ยืนยัน </a>
            </div>
        </div>
    </div>
</div>
