<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->

<html lang="{{ config('app.locale') }}">

@section('htmlheader')
    @include('layouts.partials.htmlheader')
@show
<body class="top-navigation">
<div id="wrapper">
    <div id="page-wrapper" class="gray-bg">
    @include('layouts.partials.sidebar')
    <!-- Content Wrapper. Contains page content -->
        <div id="page-wrapper" class="gray-bg">
            <!-- Main content -->
            <section class="content">
                <!-- Your Page Content Here -->
                @yield('content')
            </section><!-- /.content -->
            @include('layouts.partials.footer')
        </div><!-- ./page-wrapper -->
        @include('layouts.partials.modal')
    </div><!-- ./wrapper -->
</div><!-- ./wrapper -->

@section('scripts')
    @include('layouts.partials.scripts')
@show

</body>
</html>
