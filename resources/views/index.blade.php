@extends('layouts.app')
@section('module_name')
    Home
@endsection
@section('current')
    Index
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">

            @if ($errors->any())
                <script> toastr.error("{{ implode('', $errors->all(':message')) }}", "", {
                        "closeButton": true,
                        "debug": true,
                        "progressBar": true,
                        "preventDuplicates": false,
                        "positionClass": "toast-top-right",
                        "showDuration": "5000",
                        "hideDuration": "5000",
                        "timeOut": "5000",
                        "extendedTimeOut": "5000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                    }); </script>
            @endif

            <div class="wrapper wrapper-content">


                <div class="row">
                    <div class="col-lg-12">

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="ibox float-e-margins">
                                    <div class="ibox-title">
                                        @if(\App\User::isUser(Auth::user()->username))
                                            <a class="btn btn-primary btn-nest openDialog" data-toggle="modal"
                                               data-username="{{Auth::user()->username}}"
                                               href="#createRequest"
                                               data-status="REQUEST">
                                                <i class="fa fa-plus"></i> สร้างคำร้อง
                                            </a>
                                        @else
                                            รายการ
                                        @endif
                                    </div>
                                    <div class="ibox-content">
                                        <table class="table table-hover no-margins" id="datatable">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>สร้างเมื่อ</th>
                                                <th>ชื่อ-สกุล</th>
                                                <th>ระดับ</th>
                                                <th>แผนก</th>
                                                <th>Manager</th>
                                                <th>รายละเอียด</th>
                                                <th>จำนวน (THB)</th>
                                                <th>สถานะ</th>

                                                <th>#</th>

                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php $i = 1; ?>
                                            @foreach($items as $item)
                                                <tr>
                                                    <td>
                                                        <small>{{ $i++ }}</small>
                                                    </td>
                                                    <td>{{ $item->created_at }}</td>
                                                    <td>{{ $item->firstname }} {{ $item->lastname }}</td>
                                                    <td>{{ $item->level }}</td>
                                                    <td>{{ $item->department }}</td>
                                                    <td>{{ $item->man_firstname }} {{ $item->man_lastname }}</td>
                                                    <td>{{ $item->description }}</td>
                                                    <td>{{ number_format($item->amount, 2, '.', ',')}}</td>
                                                    <td>

                                                        @switch($item->cash_status_code)
                                                            @case("REQUEST")
                                                            <span class="label label-info">ขออนุมัติ</span>
                                                            @break

                                                            @case("APPROVE")
                                                            <span class="label label-success">อนุมัติแล้ว</span>
                                                            @break

                                                            @case("DECLINE")
                                                            <span class="label label-danger">ไม่อนุมัติ</span>
                                                            @break

                                                            @case("CANCEL")
                                                            <span class="label label-warning">ยกเลิก</span>
                                                            @break

                                                            @default
                                                            <span class="label label-inverse">เสร็จสิ้น</span>
                                                            @break

                                                        @endswitch
                                                    </td>
                                                    <td>
                                                        @if($item->cash_status_code=='REQUEST'  || $item->cash_status_code=='DECLINE')
                                                            <div class="btn-group">
                                                                @if($item->man_username == Auth::user()->username)
                                                                    @if($item->cash_status_code=='REQUEST')
                                                                        <a data-toggle="modal" href="#createRequest"
                                                                           data-status="APPROVE"
                                                                           data-username="{{$item->username}}"
                                                                           data-code="{{$item->code}}"
                                                                           data-amount="{{$item->amount}}"
                                                                           data-description="{{$item->description}}"
                                                                           class="btn btn-primary btn-flat btn-sm btn-view-item openDialog"><i
                                                                                    class="fa fa-check"></i> อนุมัติ</a>
                                                                        <a data-toggle="modal" href="#createRequest"
                                                                           data-status="DECLINE"
                                                                           data-code="{{$item->code}}"
                                                                           data-username="{{$item->username}}"
                                                                           data-amount="{{$item->amount}}"
                                                                           data-description="{{$item->description}}"
                                                                           class="btn btn-danger btn-flat btn-sm btn-view-item openDialog"><i
                                                                                    class="fa fa-close"></i> ไม่อนุมัติ</a>
                                                                    @endif
                                                                @endif
                                                                @if($item->username == Auth::user()->username)
                                                                    <a data-toggle="modal" href="#createRequest"
                                                                       data-status="EDIT"
                                                                       data-code="{{$item->code}}"
                                                                       data-username="{{$item->username}}"
                                                                       data-amount="{{$item->amount}}"
                                                                       data-description="{{$item->description}}"
                                                                       class="btn btn-info btn-flat btn-sm btn-edit-item openDialog"><i
                                                                                class="fa fa-edit"></i> แก้ไข</a>
                                                                    <a data-toggle="modal" href="#createRequest"
                                                                       data-status="CANCEL"
                                                                       data-code="{{$item->code}}"
                                                                       data-username="{{$item->username}}"
                                                                       data-amount="{{$item->amount}}"
                                                                       data-description="{{$item->description}}"
                                                                       class="btn btn-warning btn-flat btn-sm btn-edit-item openDialog"><i
                                                                                class="fa fa-trash-o"></i> ยกเลิก</a>
                                                                @endif

                                                            </div>
                                                        @else
                                                            @if($item->cash_status_code=='SUCCESS')

                                                            @else

                                                                @if($item->username == Auth::user()->username )
                                                                    @if($item->cash_status_code=='CANCEL')
                                                                    @else
                                                                        <span class="label label-inverse"> ติดต่อรับเงิน</span>
                                                                    @endif
                                                                @else
                                                                    @if($item->cash_status_code=='CANCEL')
                                                                    @else
                                                                        <a data-toggle="modal" href="#createRequest"
                                                                           data-status="SUCCESS"
                                                                           data-code="{{$item->code}}"
                                                                           data-username="{{$item->username}}"
                                                                           data-amount="{{$item->amount}}"
                                                                           data-description="{{$item->description}}"
                                                                           class="btn btn-warning btn-flat btn-sm btn-edit-item openDialog"><i
                                                                                    class="fa fa-money"></i> รับเงิน</a>
                                                                    @endif

                                                                @endif
                                                            @endif
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                        </div>


                    </div>
                </div>
            </div>

        </div>
    </div>




    <div class="modal inmodal" id="createRequest" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content animated bounceInRight">

                <form method="post" class="form-horizontal">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="modal-header">
                        <h2 id="title"></h2>
                    </div>

                    <div id="modalBody" class="modal-body">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-12">
                                    <input class="form-control" type="hidden" id="cash_status_code"
                                           name="cash_status_code">
                                    <input class="form-control" type="hidden" id="code" name="code">
                                    <input class="form-control" type="hidden" id="username" name="username">
                                    <div class="form-group">
                                        <label for="unit">จำนวนเงิน : </label>
                                        <input class="form-control" type="number" step="any" min="0"
                                               max="99999999" id="amount" name="amount">
                                    </div>
                                    <div class="form-group">
                                        <label for="description">รายละเอียด : </label>
                                        <textarea class="form-control" cols="40" rows="5" id="description"
                                                  name="description"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button id="submitButton" class="btn btn-primary" type="submit" name="submit"><i
                                    class="fa fa-save"></i> ยืนยัน
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@section('script')

    <script>
        $(document).ready(function () {
            var table = $('#datatable').DataTable({});
        });


        $(document).on("click", ".openDialog", function () {

            if ($(this).data('status') == 'EDIT' || $(this).data('status') == 'REQUEST') {
                $(".modal-body #amount").attr('disabled', false);
                $(".modal-body #description").attr('disabled', false);
            } else {
                $(".modal-body #amount").attr('disabled', true);
                $(".modal-body #description").attr('disabled', true);
            }

            switch ($(this).data('status')) {
                case 'REQUEST':
                    $(".modal-header #title").html('สร้างคำร้อง');
                    break;
                case 'APPROVE':
                    $(".modal-header #title").html('อนุมัติคำร้อง');
                    break;
                case 'DECLINE':
                    $(".modal-header #title").html('ปฏิเสธิคำร้อง');
                    break;
                case 'CANCEL':
                    $(".modal-header #title").html('ยกเลิกคำร้อง');
                    break;
                case 'EDIT':
                    $(".modal-header #title").html('แก้ไขคำร้อง');
                    break;
            }

            $(".modal-body #code").val($(this).data('code'));
            $(".modal-body #username").val($(this).data('username'));
            $(".modal-body #cash_status_code").val($(this).data('status') == 'EDIT' ? 'REQUEST' : $(this).data('status'));
            $(".modal-body #amount").val($(this).data('amount') ? $(this).data('amount') : '');
            $(".modal-body #description").val($(this).data('description') ? $(this).data('description') : '');

        });

        $('form').submit(function () {
            $(".modal-body #amount").attr('disabled', false);
            $(".modal-body #description").attr('disabled', false);

            $(this).find(':submit').attr('disabled', 'disabled');
        });
    </script>
@endsection

