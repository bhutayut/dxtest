<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use DB;
use Carbon\Carbon;

class Department extends Model
{
    protected $table = 'departments';
    protected $primaryKey = 'id';

    public static function list()
    {
        $items = DB::table('departments AS main')
            ->select('main.*')
            ->where('main.is_deleted', '<>', 1)
            ->get();

        return $items;
    }
}
