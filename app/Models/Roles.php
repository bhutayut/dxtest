<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use DB;
use Carbon\Carbon;

class Roles extends Model
{
    protected $table = 'roles';
    protected $primaryKey = 'id';

    public static function list()
    {
        $items = DB::table('roles AS main')
            ->select('main.*')
            ->where('main.is_deleted', '<>', 1)
            ->get();

        return $items;
    }
}
