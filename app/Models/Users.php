<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use DB;
use Carbon\Carbon;

class Users extends Model
{
    protected $table = 'users';
    protected $primaryKey = 'id';

    public static function list()
    {
        $items = DB::table('users AS main')
            ->select('main.*')
            ->leftjoin('titles as t1', function ($join) {
                $join->on('t1.code', 'main.title_code')->where('main.title_code', '<>', null);
            })
            ->leftjoin('user_status as t2', function ($join) {
                $join->on('t2.id', 'main.user_status_id')->where('main.user_status_id', '<>', null);
            })
            ->leftjoin('levels as t3', function ($join) {
                $join->on('t3.id', 'main.level_id')->where('main.level_id', '<>', null);
            })
            ->leftjoin('departments as t4', function ($join) {
                $join->on('t4.id', 'main.department_id')->where('main.department_id', '<>', null);
            })
            ->addselect(\db::raw('t1.name as title'))
            ->addselect(\db::raw('t2.name as user_status'))
            ->addselect(\db::raw('t3.name as level'))
            ->addselect(\db::raw('t4.name as department'))
            ->where('main.is_deleted', '<>', 1)
            ->get();

        return $items;
    }

    public static function getManager()
    {
        $items = DB::table('users AS main')
            ->select('main.*')
            ->where('main.is_deleted', '<>', 1)
            ->where('main.level_id', 3)
            ->get();

        return $items;
    }

    public static function manage($request)
    {

        $results = DB::table('users')->insert(
            [
                'username' => $request->input('username') ? $request->input('username') : null,
                'email' => $request->input('email') ? $request->input('email') : null,
                'password' => bcrypt($request->input('password')) ? bcrypt($request->input('password')) : null,

                'role_id' => $request->input('role_id') ? $request->input('role_id') : null,
                'user_status_id' => $request->input('user_status_id') ? $request->input('user_status_id') : null,
                'level_id' => $request->input('level_id') ? $request->input('level_id') : null,
                'department_id' => $request->input('department_id') ? $request->input('department_id') : null,
                'manager_code' => $request->input('manager_code') ? $request->input('manager_code') : null,

                'title_code' => $request->input('title_code') ? $request->input('title_code') : null,
                'firstname' => $request->input('firstname') ? $request->input('firstname') : null,
                'lastname' => $request->input('lastname') ? $request->input('lastname') : null,
                'nickname' => $request->input('nickname') ? $request->input('nickname') : null,

                'mobile' => $request->input('mobile') ? $request->input('mobile') : null,

                'created_by' => \Auth::user()->username,
                'updated_at' => '2000-01-01 00:00:00',
                'deleted_at' => '2000-01-01 00:00:00',

            ]
        );

        return $results;
    }
}
