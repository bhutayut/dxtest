<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use DB;
use Carbon\Carbon;

class Levels extends Model
{
    protected $table = 'levels';
    protected $primaryKey = 'id';

    public static function list()
    {
        $items = DB::table('levels AS main')
            ->select('main.*')
            ->where('main.is_deleted', '<>', 1)
            ->get();

        return $items;
    }
}
