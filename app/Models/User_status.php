<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use DB;
use Carbon\Carbon;

class User_status extends Model
{
    protected $table = 'user_status';
    protected $primaryKey = 'id';

    public static function list()
    {
        $items = DB::table('user_status AS main')
            ->select('main.*')
            ->where('main.is_deleted', '<>', 1)
            ->get();

        return $items;
    }
}
