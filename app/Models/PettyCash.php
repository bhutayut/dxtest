<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use DB;
use Carbon\Carbon;

class PettyCash extends Model
{
    protected $table = 'petty_cash';
    protected $primaryKey = 'id';

    public static function list()
    {
        $items = DB::table('petty_cash AS main')
            ->leftjoin('users as u', function ($join) {
                $join->on('u.username', 'main.username')->where('main.username', '<>', null);
            })
            ->leftjoin('users as u2', function ($join) {
                $join->on('u2.username', 'u.manager_code')->where('u.manager_code', '<>', null);
            })
            ->leftjoin('levels as t3', function ($join) {
                $join->on('t3.id', 'u.level_id')->where('u.level_id', '<>', null);
            })
            ->leftjoin('departments as t4', function ($join) {
                $join->on('t4.id', 'u.department_id')->where('u.department_id', '<>', null);
            })
            ->select('main.*')
            ->addselect(\db::raw('main.code as code'))
            ->addselect(\db::raw('main.username as username'))
            ->addselect(\db::raw('u.manager_code as man_username'))
            ->addselect(\db::raw('u.firstname as firstname'))
            ->addselect(\db::raw('u.lastname as lastname'))
            ->addselect(\db::raw('u2.firstname as man_firstname'))
            ->addselect(\db::raw('u2.lastname as man_lastname'))
            ->addselect(\db::raw('t3.name as level'))
            ->addselect(\db::raw('t4.name as department'))
            ->where('main.is_deleted', '<>', 1)
            ->get();

        return $items;
    }

    public static function summary()
    {
        $currentMonth = date('m');
        $items = DB::table('petty_cash AS main')
            ->leftjoin('users as u', function ($join) {
                $join->on('u.username', 'main.username')->where('main.username', '<>', null);
            })
            ->leftjoin('users as u2', function ($join) {
                $join->on('u2.username', 'u.manager_code')->where('u.manager_code', '<>', null);
            })
            ->leftjoin('levels as t3', function ($join) {
                $join->on('t3.id', 'u.level_id')->where('u.level_id', '<>', null);
            })
            ->leftjoin('departments as t4', function ($join) {
                $join->on('t4.id', 'u.department_id')->where('u.department_id', '<>', null);
            })
            ->addselect(\db::raw('u.id as UID'))
            ->addselect(\db::raw('main.code as code'))
            ->addselect(\db::raw('main.username as username'))
            ->addselect(\db::raw('u.manager_code as man_username'))
            ->addselect(\db::raw('u.firstname as firstname'))
            ->addselect(\db::raw('u.lastname as lastname'))
            ->addselect(\db::raw('u2.firstname as man_firstname'))
            ->addselect(\db::raw('u2.lastname as man_lastname'))
            ->addselect(\db::raw('t3.name as level'))
            ->addselect(\db::raw('t4.name as department'))
            ->addselect(\db::raw('main.amount as amount'))
            ->where('main.is_deleted', '<>', 1)
            ->whereRaw('MONTH(main.created_at) = ?', [$currentMonth])
            ->get();


        $ItemList = collect($items)->reduce(function ($array, $item) {
            if (!isset($array[$item->UID])) {
                $array[$item->UID] = array();
                $array[$item->UID]["UID"] = $item->UID;
                $array[$item->UID]["username"] = $item->username;
                $array[$item->UID]["firstname"] = $item->firstname;
                $array[$item->UID]["lastname"] = $item->lastname;

                $array[$item->UID]["man_firstname"] = $item->man_firstname;
                $array[$item->UID]["man_lastname"] = $item->man_lastname;
                $array[$item->UID]["level"] = $item->level;
                $array[$item->UID]["department"] = $item->department;

                $array[$item->UID]["amount"] = 0;
            }
            $array[$item->UID]["amount"] = $array[$item->UID]["amount"] + $item->amount;
            return $array;
        }, []);
        $items = collect($ItemList)->sortBy('UID')->all();

        return $items;
    }

    /*    public static function findByCode($code)
        {
            $items = DB::table('schools AS main')
                ->select("main.*")
                ->where('main.code', $code)
                ->where('main.is_deleted', '<>', 1)
                ->limit(1);

            $result = $items->get();

            return $result;
        }*/

    public static function manage($request)
    {
        $code = $request->input('code') ? $request->input('code') : null;
        $username = $request->input('username') ? $request->input('username') : null;
        $description = $request->input('description') ? $request->input('description') : null;
        $amount = $request->input('amount') ? $request->input('amount') : null;
        $cash_status_code = $request->input('cash_status_code') ? $request->input('cash_status_code') : null;

        DB::select(DB::raw('CALL USP_MNG_IMMATH_PETTYCASH(        
                        :code,	                        
                       	:username,
                       	:description,	                        
                       	:amount,
                       	:cash_status_code, 	 	
                     
						:mng_by,
						:device_name,				
						@o_action,
						@o_message,
						@o_id,
						@o_result);'), array(

            'code' => $code,
            'username' => $username,
            'description' => $description,
            'amount' => $amount,
            'cash_status_code' => $cash_status_code,

            'mng_by' => \Auth::user()->username,
            'device_name' => gethostbyaddr($_SERVER ['REMOTE_ADDR'])
        ));

        $results = DB::select('select @o_action as action, @o_message as message, @o_id as id, @o_result as result');
        return $results;
    }

    public static function destroy($code)
    {
        DB::select(DB::raw('CALL USP_DEL_IMMATH_PETTYCASH(
						:p_code,	    
						:deleted_by,
						:device_name,
						@o_action,
						@o_message,
						@o_id,
						@o_result);'), array(
            'p_code' => $code,
            'deleted_by' => \Auth::user()->username,
            'device_name' => gethostbyaddr($_SERVER ['REMOTE_ADDR'])
        ));
        $results = DB::select('select @o_action as action, @o_message as message, @o_id as id, @o_result as result');

        return $results;
    }
}
