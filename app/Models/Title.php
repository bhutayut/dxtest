<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use DB;
use Carbon\Carbon;

class Title extends Model
{
    protected $table = 'titles';
    protected $primaryKey = 'id';

    public static function list()
    {
        $items = DB::table('titles AS main')
            ->select('*')
            ->where('main.is_deleted', '<>', 1)
            ->get();

        return $items;
    }
}
