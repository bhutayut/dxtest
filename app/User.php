<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static function isAdmin($username)
    {
        return User::where('username', $username)->where('role_id', 1)->count();
    }

    public static function isFin($username)
    {
        return User::where('username', $username)->whereIN('role_id', [1, 4])->count();
    }

    public static function isUser($username)
    {
        return User::where('username', $username)->where('role_id', 2)->count();
    }

    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = bcrypt($password);
    }

}
