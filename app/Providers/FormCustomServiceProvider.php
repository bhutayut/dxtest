<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class FormCustomServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {


        \Form::macro('CreateInput', function ($name, $value, $class, $type, $attributes) {
            return '<div class="form-group">
                    <label class="col-xs-4 col-sm-2 col-md-5 control-label">' . str_replace('_', ' ', str_replace('ID', ' ', strtoupper(str_replace('[]', '', $name)))) . '</label>
                    <div class="col-xs-8 col-sm-10 col-md-7"><input type="' . $type . '" placeholder="' . str_replace('_', ' ', strtoupper(str_replace('[]', '', $name))) . '" class="form-control ' . $class . '"
                    name="' . $name . '" value="' . $value . '" ' . $attributes . ' /></div></div>';
        });

        \Form::macro('CreateSelect', function ($name, $groups, $items, $class, $attributes) {
            $list = null;
            foreach ($groups as $group) {
                $list .= '<option value="' . $group->id . '" ';
                if ($group->id == $items) {

                    $list .= 'selected="selected"';
                }
                $list .= ' >' . $group->name . '</option>';
            }
            return '<div class="form-group">
                    <label class="col-xs-4 col-sm-2 col-md-5 control-label">' . str_replace('_', ' ', str_replace('ID', ' ', strtoupper(str_replace('[]', '', $name)))) . '</label>
                    <div class="col-xs-8 col-sm-10 col-md-7">
                        <select class="form-control select2" name="' . $name . '" data-placeholder="' . str_replace('_', ' ', str_replace('ID', ' ', strtoupper(str_replace('[]', '', $name)))) . '" ' . $attributes . ' >
                        <option></option>
                        ' . $list . '
                        </select >
                    </div >
                    </div >';
        });
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
