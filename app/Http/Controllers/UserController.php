<?php

namespace App\Http\Controllers;

use App\Models\Roles;
use App\Models\User_status;
use DB;
use Validator;

use App\Models\Title;
use App\Models\Users;
use App\Models\Levels;
use App\Models\Department;

use Carbon\Carbon;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Users::list();

        $viewModel = array();
        $viewModel ["items"] = $items;

        return view('modules.users.index', $viewModel);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Roles::list();
        $titles = Title::list();
        $levels = Levels::list();
        $managers = Users::getManager();
        $user_status = User_status::list();
        $departments = Department::list();

        $viewModel = array();
        $viewModel ["items"] = null;
        $viewModel ["roles"] = $roles;
        $viewModel ["titles"] = $titles;
        $viewModel ["levels"] = $levels;
        $viewModel ["managers"] = $managers;
        $viewModel ["user_status"] = $user_status;
        $viewModel ["departments"] = $departments;

        return view('modules.users.create', $viewModel);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'username' => 'required|min:4|max:20',
            'email' => 'required|email',
            'password' => 'required|confirmed|min:6',
            'password_confirmation' => 'sometimes|required_with:password',
            'firstname' => 'required',
            'nickname' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect('user/create')
                ->withErrors($validator)
                ->withInput();
        }

        $model = new Users();
        $results = $model->manage($request);

        if ($results) {
            return redirect()->action('UserController@index');
        } else {
            return redirect()->action('UserController@index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
