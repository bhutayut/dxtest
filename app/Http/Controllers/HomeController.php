<?php

namespace App\Http\Controllers;

use App\Models\PettyCash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $action = PettyCash::list();

        $viewModel = array();
        $viewModel ["items"] = $action;

        return view('index', $viewModel);


    }

    public function store(Request $request)
    {
        $model = new PettyCash();
        $results = $model->manage($request);

        if ($results[0]->result) {
            return redirect()->action('HomeController@index')->with($results [0]->message, $results [0]->action . ' ' . $results [0]->message);
        } else {
            return redirect()->action('HomeController@index')->with($results [0]->message, $results [0]->action . ' ' . $results [0]->message);
        }

    }

}
